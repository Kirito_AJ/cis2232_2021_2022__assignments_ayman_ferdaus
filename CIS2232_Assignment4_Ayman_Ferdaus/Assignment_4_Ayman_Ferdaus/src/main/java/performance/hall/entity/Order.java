package performance.hall.entity;

import info.hccis.util.CisUtility;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Order {

    public static final double HollPass_Discount = 0.10;
    public static final double Ten_Or_More_Tickets = 0.10;
    public static final double Twenty_Or_More_Tickets = 0.15;

    //Private attributes
    private int numberOfTickets;
    private boolean hasHollpass;
    private double discount;
    private double cost;
    private String name;
    private int hallPassNumber;
    private int regularCost;
    private double averageCost;

    public Order() {

    }

    public Order(int numberOfTickets, boolean hasHollpass) {
        this.numberOfTickets = numberOfTickets;
        this.hasHollpass = hasHollpass;
    }

    public double calculateCost() {
        int ticketPrice = 10;
        regularCost = numberOfTickets * ticketPrice;
        double sale = 0;
        if (hasHollpass) {
            sale += getDiscount();
        }
        if (numberOfTickets < 10) {
            cost = regularCost - (regularCost * sale);
        } else if (numberOfTickets < 20) {
            sale += .1;
            cost = regularCost - (regularCost * sale);
        } else {
            sale += .15;
            cost = regularCost - (regularCost * sale);
        }
        return cost;
    }

    public static boolean validatee(int hollPassNumber) {
        if (hollPassNumber % 13 == 7) {
            return true;
        } else {
            return false;
        }
    }

    public double getAverageCost() {
        return averageCost = (cost / numberOfTickets);
    }

    public void setAverageCost(double averageCost) {
        this.averageCost = averageCost;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public boolean isHasHollpass() {
        return hasHollpass;
    }

    public void setHasHollpass(boolean hasHollpass) {
        this.hasHollpass = hasHollpass;
    }

    public double getDiscount() {
        discount = .1;
        return discount;
    }

    public double getCost() {
        return cost;
    }

    public void getInformation(boolean useConsole) {
        numberOfTickets = CisUtility.getInputInt("Enter number of tickets:", useConsole);
        String hollpassYN = CisUtility.getInputString("Do you have a Hollpass? (y/n)", useConsole);

        hasHollpass = hollpassYN.equalsIgnoreCase("Y") ? true : false;
        //OR could do it this way
        if (hollpassYN.equalsIgnoreCase("Y")) {
            hasHollpass = true;
        } else {
            hasHollpass = false;
        }
    }

    /**
     * This method is used to validate the HallPass Number.
     *
     * @since Dec 03, 2021
     * @author fayman
     */
    public void validate(int hallPassNumber) {

        boolean valid = true;
        do {
            valid = true;
            try {
                hallPassNumber = CisUtility.getInputInt("Please enter your HollPass number: ");

                if (hallPassNumber < 0) {
                    valid = false;
                    System.out.println("Invalid Entry\n");
                }
                if (!(hallPassNumber % 13 == 0)) {
                    System.out.println("Please enter a valid HollPass number that is divisible by 13." + "\n");
                    valid = false;
                }
            } catch (Exception e) {
                valid = false;
                System.out.println("Invalid Entry\n");
            }
        } while (!valid);

    }

    /**
     * Get input from the user.
     *
     * @since Dec 03, 2021
     * @author fayman
     */
    public void processOrder() {
        boolean valid = true;
        do {
            valid = true;

            name = CisUtility.getInputString(System.lineSeparator() + "Enter name: ");
            if (name.isBlank()) {
                valid = false;
                System.out.println("Invalid Entry\n");
            }
        } while (!valid);

        do {
            valid = true;
            try {
                numberOfTickets = CisUtility.getInputInt("Enter number of tickets: ");
                if (numberOfTickets < 0) {
                    valid = false;
                    System.out.println("Invalid Entry\n");
                }
            } catch (Exception e) {
                valid = false;
                System.out.println("Invalid Entry\n");
            }
        } while (!valid);

        do {
            valid = true;
            try {
                String hollpassYN = CisUtility.getInputString("Do you have a Hollpass? (yes/no)");

                if (hollpassYN.isBlank()) {
                    valid = false;
                    System.out.println("Invalid Entry\n");
                }

                if (hollpassYN.equalsIgnoreCase("YES")) {
                    hasHollpass = true;
                    validate(hallPassNumber);
                } else {
                    hasHollpass = false;
                }

            } catch (Exception e) {
                valid = false;
                System.out.println("Invalid Entry!!\n");
            }

        } while (!valid);

    }

    /**
     * Displays the order detail.
     *
     * @since Dec 03, 2021
     * @author fayman
     */
    public void display() {
        CisUtility.display(toString());
    }

    public void display(boolean useConsole) {
        if (useConsole) {
            System.out.println(toString());
        } else {
            JOptionPane.showMessageDialog(null, toString());
        }

    }

    @Override
    public String toString() {
        System.out.println(System.lineSeparator() + "Thank you for your order!");
        return "Name: " + name + "\n" + "Number of tickets:" + numberOfTickets + "\n" + "Regular cost: $" + regularCost + "\n"
                + "Discount: " + discount + "\n" + "Cost: $" + cost + "\n";
    }

}
