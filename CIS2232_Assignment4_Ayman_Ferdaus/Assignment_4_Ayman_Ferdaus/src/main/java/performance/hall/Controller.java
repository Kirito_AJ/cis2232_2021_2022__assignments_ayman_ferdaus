package performance.hall;

import info.hccis.util.CisUtility;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import performance.hall.entity.Order;

/**
 * A program for Florence Simmons Hall Ticket Office Application.
 *
 * @author fayman
 * @since Dec 03, 2021
 */
public class Controller {

    public static final String MENU = "Florence Simmons Hall Ticket Office Application" + System.lineSeparator()
            + "1) Make an order for tickets" + System.lineSeparator()
            + "2) Show summary" + System.lineSeparator()
            + "0) Exit" + System.lineSeparator();

    private static ArrayList<Order> orders = new ArrayList();

    public static void main(String[] args) {

        String option = "";

        while (!option.equalsIgnoreCase("0")) {
            option = CisUtility.getInputString(MENU);
            option = option.toUpperCase();
            switch (option) {
                case "1":
                    orderForTickets();
                    break;
                case "2":
                    showSummary();
                    break;
                case "0":
                    System.out.println(System.lineSeparator() + "Thanks for using our program - Have a good day!!!");
                    break;
                default:
                    System.out.println("Invalid option");
            }
        }

    }

    public static void orderForTickets() {
        Order order = new Order();
        order.processOrder();
        order.calculateCost();
        order.display();
        orders.add(order);
    }

    public static void showSummary() {
        double totalSales = 0;
        for (Order current : orders) {
            totalSales += current.getCost();
        }
        double averageCost = 0;
        for (Order current : orders) {
            averageCost += current.getAverageCost();
        }
        int numberOfTickets = 0;
        for (Order current : orders) {
            numberOfTickets += current.getNumberOfTickets();
        }
        System.out.println(System.lineSeparator() + "Summary");
        System.out.println("Total sales: $" + totalSales);
        System.out.println("Tickets sold: " + numberOfTickets);
        System.out.println("Average Cost: $" + averageCost + "  per ticket" + System.lineSeparator());
    }

}
