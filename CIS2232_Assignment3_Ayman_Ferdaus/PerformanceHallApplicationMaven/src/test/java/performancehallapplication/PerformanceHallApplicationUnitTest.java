package performancehallapplication;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * A unit test class is created to validate the input of the software.
 *
 * @author fayman
 * @since 20211126
 */
public class PerformanceHallApplicationUnitTest {

    //Test Method One
    //Cost price is calculated when 0 number of tickets are purchased.
    @Test
    public void test_calculateCost_0_tickets() {
        Order order = new Order();
        order.setNumberOfTickets(0);
        double costActual = order.calculateCost();
        double costExpected = 0;
        Assertions.assertEquals(costExpected, costActual);
    }

    //Test Method Two
    //Cost price is calculated when 15 number of tickets are purchased and
    //they don't have HallPass.
    @Test
    public void test_calculateCost_15_tickets() {
        Order order = new Order();
        order.setHasHollpass(false);
        order.setNumberOfTickets(15);
        double costActual = order.calculateCost();
        double costExpected = 135;
        Assertions.assertEquals(costExpected, costActual);
    }

    //Test Method Three
    //Cost price is calculated when 25 number of tickets are purchased and
    //they don't have HallPass.
    @Test
    public void test_calculateCost_25_tickets() {
        Order order = new Order();
        order.setHasHollpass(false);
        order.setNumberOfTickets(25);
        double costActual = order.calculateCost();
        double costExpected = 225;
        Assertions.assertEquals(costExpected, costActual);
    }

    //Test Method Four
    //Cost price is calculated when 12 number of tickets are purchased and
    //they have HallPass.
    @Test
    public void test_calculateCost_12_tickets() {
        Order order = new Order();
        order.setHasHollpass(true);
        order.setNumberOfTickets(12);
        double costActual = order.calculateCost();
        double costExpected = 96;
        Assertions.assertEquals(costExpected, costActual);
    }

    //Test Method Five
    //Cost price is calculated when 5 number of tickets are purchased.
    @Test
    public void test_calculateCost_5_tickets() {
        Order order = new Order();
        order.setHasHollpass(true);
        order.setNumberOfTickets(5);
        double costActual = order.calculateCost();
        double costExpected = 45;
        Assertions.assertEquals(costExpected, costActual);
    }

    //Test Method Six
    //Validate method is used to validate the hallPassNumber is correct within 7 digits.
    @Test
    public void test_hallpass_number_within_seven_digits() {
        Order order = new Order();
        order.setHollPassNumber(13);
        boolean validationActual = order.validate(13);
        boolean validationExpected = true;
        Assertions.assertEquals(validationExpected, validationActual);
    }

    //Test Method Seven
    //Validate method is used to validate the hallPassNumber divisible by 13.
    @Test
    public void test_hallpass_number_divisible_by_13() {
        Order order = new Order();
        order.setHollPassNumber(777777);
        boolean validationActual = order.validate(777777);
        boolean validationExpected = true;
        Assertions.assertEquals(validationExpected, validationActual);
    }

    //Test Method Eight
    //Validate method is used to validate the hallPassNumber cannot be more than 7 digits.
    @Test
    public void test_hallpass_number_more_than_seven_digits() {
        Order order = new Order();
        order.setHollPassNumber(99999999);
        boolean validationActual = order.validate(99999999);
        boolean validationExpected = false;
        Assertions.assertEquals(validationExpected, validationActual);
    }

    //Test Method Nine
    //Validate method is used to validate the hallPassNumber cannot be negative.
    @Test
    public void test_hallpass_number_negative() {
        Order order = new Order();
        order.setHollPassNumber(-1);
        boolean validationActual = order.validate(-1);
        boolean validationExpected = false;
        Assertions.assertEquals(validationExpected, validationActual);
    }

    //Test Method Ten
    //Validate method is used to validate the hallPassNumber is valid and not divisible by 13.
    @Test
    public void test_hallpass_number_isValid_and_not_divisible_by_13() {
        Order order = new Order();
        order.setHollPassNumber(7777777);
        boolean validationActual = order.validate(7777777);
        boolean validationExpected = false;
        Assertions.assertEquals(validationExpected, validationActual);
    }

}
