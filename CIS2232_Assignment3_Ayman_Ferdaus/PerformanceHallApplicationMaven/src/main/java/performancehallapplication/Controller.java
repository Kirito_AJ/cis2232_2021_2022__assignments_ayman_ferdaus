/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package performancehallapplication;

/**
 * Sample program for Test Driven Development
 * @author bjmaclean
 * @since 20211122
 */
public class Controller {

    public static void main(String[] args) {
        
        Order order = new Order();
        order.getInformation();
        order.display();
        
    }
    
}
