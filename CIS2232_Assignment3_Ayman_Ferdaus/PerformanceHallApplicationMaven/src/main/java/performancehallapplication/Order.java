package performancehallapplication;

import java.util.Scanner;

/**
 * A software created to sell the tickets for CIS Performance Hall.
 *
 * @author fayman
 * @since 20211126
 */
public class Order {

    public static final int Individual_Ticket_Price = 10;
    public static final double HollPass_Discount = 0.10;
    public static final double Ten_Or_More_Tickets = 0.10;
    public static final double Twenty_Or_More_Tickets = 0.15;

    private int numberOfTickets;
    private boolean hasHollpass;
    private double discount;
    private double cost;

    //Derived attributes
    private double ticketCost;
    private double hollPassNumber;

    public Order() {
        //Default
    }

    public Order(int numberOfTickets, boolean hasHollpass, double discount, double cost, double hallPassNumber) {
        this.numberOfTickets = numberOfTickets;
        this.hasHollpass = hasHollpass;
        this.discount = discount;
        this.cost = cost;
        this.hollPassNumber = hallPassNumber;
    }

    /**
     * Calculate the cost of the tickets.
     *
     * @author fayman
     * @since 20211126
     */
    public double calculateCost() {
        double ticketCost = 0;
        if (hasHollpass == true) {
            ticketCost = (Individual_Ticket_Price * numberOfTickets) - ((Individual_Ticket_Price * numberOfTickets) * HollPass_Discount);
        }
        if (numberOfTickets >= 0 || numberOfTickets <= 10) {
            System.out.println("Less than 10 tickets");
            ticketCost = (Individual_Ticket_Price * numberOfTickets);
        }
        if (numberOfTickets > 20 && hasHollpass == true) {
            ticketCost = (Individual_Ticket_Price * numberOfTickets) - ((Individual_Ticket_Price * numberOfTickets) * (Twenty_Or_More_Tickets + HollPass_Discount));
        } else {
            System.out.println("No HallPass");
            ticketCost = (Individual_Ticket_Price * numberOfTickets) - ((Individual_Ticket_Price * numberOfTickets) * (Twenty_Or_More_Tickets));
        }

        if (numberOfTickets > 10 && numberOfTickets < 20 && hasHollpass == true) {
            ticketCost = (Individual_Ticket_Price * numberOfTickets) - ((Individual_Ticket_Price * numberOfTickets) * (Ten_Or_More_Tickets + HollPass_Discount));
        } else {
            ticketCost = (Individual_Ticket_Price * numberOfTickets) - ((Individual_Ticket_Price * numberOfTickets) * Ten_Or_More_Tickets);
        }

        return ticketCost;
    }

    /**
     * Validating a HallPass Number.
     *
     * @author fayman
     * @since 20211126
     */
    public static boolean validate(int hollPassNumber) {

        boolean isValid = true;
        if (hollPassNumber < 0 || hollPassNumber > 9999999) {
            System.out.println("Invalid HollPass Number. Try Again.");
            isValid = false;
        } else {
            System.out.println("Valid HallPass Number.");
            isValid = true;
        }
        
        if (hollPassNumber % 13 == 0) {
            System.out.println("HallPass Number is divisible by 13.");
            isValid = true;
        } else {
            System.out.println("HallPass Number is not divisible by 13.");
            isValid = false;
        }
        
        return isValid;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public boolean isHasHollpass() {
        return hasHollpass;
    }

    public void setHasHollpass(boolean hasHollpass) {
        this.hasHollpass = hasHollpass;
    }

    public double getDiscount() {
        return discount;
    }

    public double getCost() {
        return cost;
    }

    public double getHollPassNumber() {
        return hollPassNumber;
    }

    public void setHollPassNumber(double hollPassNumber) {
        this.hollPassNumber = hollPassNumber;
    }

    public void getInformation() {
        System.out.println("Enter number of tickets:");
        Scanner input = new Scanner(System.in);
        numberOfTickets = input.nextInt();
        input.nextLine();
        System.out.println("Do you have a Hollpass? (y/n)");
        String hollpassYN = input.nextLine();

        hasHollpass = hollpassYN.equalsIgnoreCase("Y") ? true : false;
        //OR could do it this way
        if (hollpassYN.equalsIgnoreCase("Y")) {
            hasHollpass = true;
        } else {
            hasHollpass = false;
        }
        System.out.println("Enter your HallPass Number?");
        hollPassNumber = input.nextDouble();
        input.nextLine();//Burn the line

    }

    public void display() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Order: # Tickets: " + numberOfTickets + "\nHas Holl Pass:" + hasHollpass + "\nDiscount: " + discount + "\nCost:" + cost;
    }

}
