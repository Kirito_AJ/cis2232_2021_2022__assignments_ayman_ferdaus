package info.hccis.tutor;

import com.google.gson.Gson;
import info.hccis.tutor.bo.Business;
import info.hccis.tutor.bo.Employee;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * A project to track tutor transactions.
 *
 * @author bjmaclean
 * @since 20200617
 * @modified by fayman 20210924 A file is created to save the employees information using json. The employees information are later 
 * uploaded to the system when the program is started.
 */
public class Controller {

    private static final String FOLDER_NAME = "c:/cis2232/tutors.txt";
    private static final String FILE_NAME = "tutors.json";

    public static void main(String[] args) {

        Business.showBusinessInformation();
        System.out.println(""); //An empty line is created.
        String fullFileName = FOLDER_NAME + "\\" + FILE_NAME;
        try {
            Files.createDirectories(Paths.get(FOLDER_NAME));
        } catch (IOException ex) {
            System.out.println("There was an error in creating the directories.");
        }

        ArrayList<String> linesFromFile = null;

        try {
            linesFromFile = (ArrayList) Files.readAllLines(Paths.get(fullFileName));
        } catch (IOException ex) {
            System.out.println("There was an error in reading the file");
        }

        //For each line an employee object is created and added to an ArrayList of Employees.
        ArrayList<Employee> employees = new ArrayList();

        boolean foundEmployees = linesFromFile != null;

        Gson gson = new Gson();

        if (foundEmployees) {
            System.out.println("Loading employees from file");
            for (String current : linesFromFile) {
                if (current.length() > 1) {
                    System.out.println(current);

                    Employee employee = gson.fromJson(current, Employee.class);

                    employees.add(employee);
                }
            }
            System.out.println(""); //An empty line is created.
            System.out.println("Employees are uploaded to the system. Total of " + employees.size() + " employees are added to the list.");
        } else {
            System.out.println("There are no employees available to load.");
        }

        //Showing the employees that were uploaded.
        System.out.println(""); //An empty line is created.
        System.out.println("--------------------------------------------------------");
        System.out.println("- Here are the employees that were uploaded from the file.");
        System.out.println("--------------------------------------------------------");
        System.out.println(""); //An empty line is created.
        for (Employee current : employees) {
            System.out.println(current.toString());
        }

        System.out.println("--------------------------------------------------------");
        System.out.println("- Continued..............");
        System.out.println("--------------------------------------------------------");

        System.out.println(""); //An empty line is created.
        
        //Creating a new Employee
        Employee employee = new Employee();
        employee.getInformtion();

        //Writing the new name to the file after getting the user input.
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(fullFileName, true);
            fileWriter.write(gson.toJson(employee) + System.lineSeparator());
            fileWriter.flush();
            
        } catch (IOException ex) {
            System.out.println("There was an error in writing the name of the employee to the file.");
            ex.printStackTrace();
        }

        System.out.println("Thank you for using the program. Have a wonderful day.  ");

    }
}
