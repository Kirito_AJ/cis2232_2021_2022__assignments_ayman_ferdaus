package info.hccis.starting.bo;

import info.hccis.util.UtilityRest;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This method process the Api service.
 *
 * @author fayman
 * @since Nov 11, 2021
 */
public class ApiProcessor {

    public static final String URL = "http://dummy.restapiexample.com/api/v1/employees";

    public static int callApi(int idEntered) {

        int employeeSalary = 0;

        String json;
        try {
            json = UtilityRest.getJsonFromRest(URL);
            JSONObject jsonObjectOuter = new JSONObject(json);
            JSONArray jsonArray = jsonObjectOuter.getJSONArray("data");
            boolean found = false;
            for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {

                JSONObject jsonObjectForID = jsonArray.getJSONObject(currentIndex);
                int idFound = jsonObjectForID.getInt("id");
                if (idFound == idEntered) {
                    System.out.println("Found the User ID");
                    employeeSalary = jsonObjectForID.getInt("employee_salary");
                    found = true;
                    break;
                }
            }
            if (!found) {
                System.out.println("Could not find employee salary for that userID");
                employeeSalary = -1;
            }

        } catch (Exception ex) {
            Logger.getLogger(ApiProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return employeeSalary;
    }

}
