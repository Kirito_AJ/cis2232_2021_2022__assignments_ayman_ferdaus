package info.hccis.starting;

import info.hccis.starting.bo.ApiProcessor;
import info.hccis.util.CisUtility;
import java.util.Scanner;

/**
 * Main controller class calls the API service method to get the user input and
 * display the result.
 *
 * @author fayman
 * @since Nov 11, 2021
 */
public class Controller {

    public static final String MENU = "API Service" + System.lineSeparator()
            + "1) Enter Employee Information" + System.lineSeparator()
            + "0) Exit" + System.lineSeparator();

    public static void main(String[] args) {
        System.out.println("Welcome (" + CisUtility.getTodayString("yyyy-MM-dd") + ")");
        System.out.println("");
        System.out.println("");

        String option = "";

        while (!option.equalsIgnoreCase("0")) {
            option = CisUtility.getInputString(MENU);
            option = option.toUpperCase();
            switch (option) {
                case "1":
                    apiService();
                    break;
                case "0":
                    System.out.println(System.lineSeparator() + "Thanks for using our program - Have a good day!!!");
                    break;
                default:
                    System.out.println("Invalid option" + System.lineSeparator());
            }
        }

    }

    public static void apiService() {
        int userID = CisUtility.getInputInt("Enter User ID?");
        int employeeSalary = ApiProcessor.callApi(userID);
        if (employeeSalary > 0) {
            System.out.println("ID = " + userID + " Employee has an yearly income of $" + employeeSalary + System.lineSeparator());
        } else {
            System.out.println("Could not find the user ID ");
        }
    }

}
