package info.hccis.sample.bo;

import info.hccis.sample.dao.ReportStudentCourseDAO;

import java.util.ArrayList;

/**
 * Student Business Object
 *
 * @author fayman
 * @since Dec 07, 2021
 */
public class StudentBO {

    public ArrayList<String> determineStartingHollPassBalance(String program) {

        ReportStudentCourseDAO rscdao = new ReportStudentCourseDAO();
        return rscdao.determineStartingHollPassBalance(program);

    }
}
