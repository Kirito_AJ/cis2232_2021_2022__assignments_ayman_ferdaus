package info.hccis.sample.bo;

import info.hccis.sample.dao.ReportCoursesForStudentDAO;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class ReportCoursesForStudentBO {

    public ArrayList<String> selectCoursesForStudent(int id) {

        ReportCoursesForStudentDAO rcfsdao = new ReportCoursesForStudentDAO();
        return rcfsdao.selectCoursesForStudent(id);
    
    }

    ArrayList<String> determineStartingHollPassBalance(String program) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}