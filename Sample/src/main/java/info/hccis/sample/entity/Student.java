package info.hccis.sample.entity;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bjmaclean
 * @since Oct 28, 2021
 * @modified by fayman Dec 07, 2021 An attribute for hollPassBalance is added
 * alongside with its getter and setter.
 */
@Entity
@Table(name = "student")
@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")})
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "studentId")
    private Integer studentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 100)
    @Column(name = "program")
    private String program;
    @Size(max = 10000)
    @Column(name = "hollPassBalance")
    private double hollPassBalance;

    public Student() {
    }

    public Student(Integer studentId) {
        this.studentId = studentId;
    }

    public Student(Integer studentId, String name) {
        this.studentId = studentId;
        this.name = name;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    //Getters and Setters for hollPassBalance.
    public double getHollPassBalance() {
        return hollPassBalance;
    }

    public void setHollPassBalance(double hollPassBalance) {
        this.hollPassBalance = hollPassBalance;
    }

    public void determineStartingHollPassBalances(String program) {
        int CIS = 1000;
        int CNET = 750;
        int anyOtherProgram = 500;
        if (getProgram() == "CIS") {
            setHollPassBalance(CIS);
        } else if (getProgram() == "CNET") {
            setHollPassBalance(CNET);
        } else {
            setHollPassBalance(anyOtherProgram);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentId != null ? studentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.studentId == null && other.studentId != null) || (this.studentId != null && !this.studentId.equals(other.studentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.sample.entity.Student[ studentId=" + studentId + " ]";
    }

}
