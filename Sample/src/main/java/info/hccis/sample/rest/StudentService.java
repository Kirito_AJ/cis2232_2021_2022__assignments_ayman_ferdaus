package info.hccis.sample.rest;

import com.google.gson.Gson;
import info.hccis.sample.entity.Student;
import info.hccis.sample.repositories.StudentRepository;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/StudentService/student")
public class StudentService {

    private final StudentRepository _sr;

    @Autowired
    public StudentService(StudentRepository _sr) {
        this._sr = _sr;
    }

    @GET
    @Produces("application/json")
    public ArrayList<Student> getAll() {
        ArrayList<Student> students = (ArrayList<Student>) _sr.findAll();
        return students;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getStudentById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Student> student = _sr.findById(id);

        if (!student.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(student).build();
        }
    }

    /**
     * This rest service returns student based on their program.
     * 
     * @author bjmaclean
     * @since Oct 28, 2021
     * @modified by fayman Dec 07, 2021 Enhanced the rest service to return the student name 
     * based on their program.
     */
    @GET
    @Path("/program/{program}")
    @Produces("application/json")
    public ArrayList<Student> getStudentsByProgram(@PathParam("program") String program) {

        ArrayList<Student> students = new ArrayList();
        Student student = new Student();
        student.getName();
        if (student.getName() == null) {
            student.setName("");
        }
        student.getStudentId();
        if (student.getStudentId() == null) {
            student.setStudentId(0);
        }
        student.getHollPassBalance();
        student.setProgram(program);
        students.add(student);
        return students;
    }

    public String save(String json) throws AllAttributesNeededException {

        Gson gson = new Gson();
        Student student = gson.fromJson(json, Student.class);

        if (student.getStudentId() == null) {
            student.setStudentId(0);
        }

        student = _sr.save(student);

        String temp = "";
        temp = gson.toJson(student);

        return temp;

    }

}
