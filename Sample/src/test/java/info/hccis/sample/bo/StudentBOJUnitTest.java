package info.hccis.sample.bo;

import info.hccis.sample.entity.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author UPDATE * Student BO Unit Tests
 *
 * @author UPDATE
 * @since UPDATE
 * @modified by fayman Dec 07, 2021 Two unit tests are created to test the
 * StudentBO method.
 */
public class StudentBOJUnitTest {

    public StudentBOJUnitTest() {
    }

// TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    // The methods must also start wtih the word test !!!!!!!!
    /**
     * Unit test created to test the hollPassBalance at 750.
     *
     * @author fayman
     * @since Dec 07, 2021
     */
    @Test
    public void test_hollPassBalance_750() {
        Student student = new Student();
        student.setProgram("CNET");
        student.setHollPassBalance(0);
        double hollPassBalanceActual = student.getHollPassBalance();
        double hollpassBalanceExpected = 0;
        Assertions.assertEquals(hollpassBalanceExpected, hollPassBalanceActual);
    }

    /**
     * Unit test created to test the hollPassBalance at 1000.
     *
     * @author fayman
     * @since Dec 07, 2021
     */
    @Test
    public void test_hollPassBalance_1000() {
        Student student = new Student();
        student.setProgram("CIS");
        student.setHollPassBalance(0);
        double hollPassBalanceActual = student.getHollPassBalance();
        double hollpassBalanceExpected = 0;
        Assertions.assertEquals(hollpassBalanceExpected, hollPassBalanceActual);
    }
}
